const { Telegraf } = require('telegraf');
const bot = new Telegraf('INSERT_TELEGRAF_KEY');
const Web3 = require('web3');

// moralis options
const options = {
    // Enable auto reconnection
    reconnect: {
      auto: true,
      delay: 5000, // ms
      maxAttempts: 10,
      onTimeout: false
    }
  };

//method for invoking start command
bot.command('startwhalealert', ctx => {
    console.log(ctx.from)
    bot.telegram.sendMessage(ctx.chat.id, 'hello there! booting up whale tracker...', {
    })

    // Ethereum Watcher
    // ================
    class TransactionCheckerEth {
        web3;
        web3ws;
        account;
        subscription;
    
        constructor(projectId, account) {
            this.web3ws = new Web3(new Web3.providers.WebsocketProvider('wss://mainnet.infura.io/ws/v3/' + projectId));
            this.web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/v3/' + projectId));
            this.account = account.toLowerCase();
        }
    
        subscribe(topic) {
            this.subscription = this.web3ws.eth.subscribe(topic, (err, res) => {
                if (err) console.error(err);
            });
        }
    
        watchTransactions() {
            console.log('Watching all pending transactions...');
            this.subscription.on('data', (txHash) => {
                setTimeout(async () => {
                    try {
                        let tx = await this.web3.eth.getTransaction(txHash);
                        if (tx != null) {
                            if (this.account == tx.to.toLowerCase()) {
                                bot.telegram.sendMessage(ctx.chat.id, '🐳  New Eth Movement!' + '\naddress: ' + tx.from + '\nvalue: ' + this.web3.utils.fromWei(tx.value, 'ether') + '\ntimestamp: ' + new Date());
                                console.log({address: tx.from, value: this.web3.utils.fromWei(tx.value, 'ether'), timestamp: new Date() });
                            }
                        }
                    } catch (err) {
                        console.error(err);
                    }
                }, 70000)
            });
        }
    }

    
    
    // start checker eth chain
    let txCheckerEth = new TransactionCheckerEth('', ''); // insert address and method
    txCheckerEth.subscribe('pendingTransactions');
    txCheckerEth.watchTransactions();
    


})

bot.launch();